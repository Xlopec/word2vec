"""
This program runs machine learning process in order to solve a classification problem.
The problem is to find and extract definitions from raw text.

Program args:
[positive training set file] [negative training set file]
"""

import sys

import numpy as np
import pandas as pd
from pandas import DataFrame
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split

import Word2Vec
from functions import load_model


def __mk_data_frame__(model: Word2Vec, label: float) -> DataFrame:
    model.init_sims(replace=True)

    d = {}

    for w in model.index2word:
        d[w] = np.append(model.syn0norm[model.vocab[w].index], label)

    return DataFrame(
        data=d.values(),
        index=d.keys(),
        columns=['Feature #{}'.format(i) for i in range(1, model.vector_size + 1)] + ['label']
    )


def __logistic_regression__(x_train, y_train) -> LogisticRegression:
    logreg = LogisticRegression(verbose=1)
    logreg.fit(x_train, y_train)

    return logreg


def main():
    print("Program args={}".format(sys.argv))

    merged_df: DataFrame = pd.concat([
        __mk_data_frame__(load_model(sys.argv[1], False), 1.0),
        __mk_data_frame__(load_model(sys.argv[2], False), 0.0)
    ])

    print("merged values")

    y = merged_df['label']

    X_train, X_test, y_train, y_test = train_test_split(merged_df, y, test_size=0.33)

    from sklearn.preprocessing import MinMaxScaler

    scaler = MinMaxScaler()
    X_train = scaler.fit_transform(X_train)
    X_test = scaler.transform(X_test)

    logreg = __logistic_regression__(X_train, y_train)

    print('Accuracy of Logistic regression classifier on training set: {:.2f}'
          .format(logreg.score(X_train, y_train)))
    print('Accuracy of Logistic regression classifier on test set: {:.2f}'
          .format(logreg.score(X_test, y_test)))

    print(X_test)

    print("Predicted labels for X_test {}".format(logreg.predict(X_test)))
    print("Score {}".format(logreg.score(X_test, y_test)))

    from sklearn.model_selection import cross_val_score

    k = 4

    scores = cross_val_score(logreg, merged_df, y, cv=k)

    print("Scores after {}-fold validation are {}".format(k, scores))

if __name__ == "__main__":
    main()
