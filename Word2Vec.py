"""
    Run this program to prepare and store the data set.
    Program args:
    [mode] [dir with json files] [out file] [should store in binary format?]. For example:
    train input/positive out/positive.txt False
    """

import logging
import sys

from typing import List
from gensim.models import Word2Vec
from functions import load_model
from functions import train_model

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


def prepare_model(args: List[str]) -> Word2Vec:
    if args[1] == "train":
        # directory, out file, is binary
        return train_model(args[2], args[3])

    if args[1] == "read":
        return load_model(args[2], (sys.argv[3:] + [False])[0])

    raise ValueError("Invalid args")


if __name__ == "__main__":
    print("The program args={}".format(sys.argv))

    model: Word2Vec = prepare_model(sys.argv)

    print(model.most_similar("місто"))
