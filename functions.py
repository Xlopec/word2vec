import functools
import json
import logging

from gensim.models import Word2Vec
from operator import iconcat as concat
from os import listdir
from os.path import join
from typing import List

from gensim.utils import simple_preprocess

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


def train_model(directory: str, out_file: str) -> Word2Vec:
    data_set = read_files(directory)

    model = Word2Vec(data_set, size=10, iter=10, workers=4, batch_words=len(data_set) / 4)

    logging.info("Starting training model")
    model.train(data_set, total_examples=len(data_set))

    logging.info("Storing model to file {}".format(out_file))
    model.save_word2vec_format(fname=out_file, binary=False)

    return model


def load_model(file_path: str, binary: bool = False) -> Word2Vec:
    return Word2Vec.load_word2vec_format(file_path, binary=binary)


def read_files(dir_path: str) -> List[List[str]]:
    files = listdir(dir_path)
    print("Parsing all JSON files={} from dir={}".format(files, dir_path))

    definitions = __read_files__(files, dir_path)

    logging.info("Done reading definitions={}".format(definitions))

    return definitions


def __read_files__(files: List[str], directory: str) -> List[List[str]]:
    return functools.reduce(concat,
                            [[s.split(" ") for s in
                              __read_file__(join(directory, f))]
                             for f in files], [])


def __read_file__(file: str) -> List[str]:
    with open(file, "r", encoding="utf-8") as f:
        data = [item['text'] for item in json.load(f)]

    return data
